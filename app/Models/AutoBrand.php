<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoBrand extends Model
{
    public $fillable = ['id','brand_name'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoMods(){
        return $this->hasMany(AutoMod::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAutos(){
        return $this->hasMany(UserAuto::class);
    }
}
