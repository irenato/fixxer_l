<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoGeneration extends Model
{
    public $fillable = ['id', 'model_id', 'generation_name', 'year_begin', 'year_end'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoMod(){
        return $this->belongsTo(AutoMod::class);
    }
}
