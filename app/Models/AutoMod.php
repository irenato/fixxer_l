<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoMod extends Model
{
    public $fillable = ['id', 'brand_id', 'model_name'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoBrand(){
        return $this->belongsTo(AutoBrand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoModification(){
        return $this->hasMany(AutoModification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoSeries(){
        return $this->hasMany(AutoSeries::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAutos(){
        return $this->hasMany(UserAuto::class);
    }
}
