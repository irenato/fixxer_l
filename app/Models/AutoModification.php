<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoModification extends Model
{
    public $fillable = ['id', 'series_id', 'modification_name', 'start_production_year', 'end_production_year'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoMod(){
        return $this->belongsTo(AutoMod::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoSeries(){
        return $this->belongsTo(AutoSeries::class);
    }
}
