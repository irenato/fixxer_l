<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoSeries extends Model
{
    public $fillable = ['id', 'model_id', 'series_name'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoMod(){
        return $this->belongsTo(AutoMod::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoSeries(){
        return $this->belongsTo(AutoSeries::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoModifications(){
        return $this->hasMany(AutoModification::class);
    }
}
