<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpeedStatus extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
