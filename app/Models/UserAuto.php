<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAuto extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoBrand(){
        return $this->belongsTo(AutoBrand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoGeneration(){
        return $this->belongsTo(AutoGeneration::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoMod(){
        return $this->belongsTo(AutoMod::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoModification(){
        return $this->belongsTo(AutoModification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoSeries(){
        return $this->belongsTo(AutoSeries::class);
    }
}
