<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPhone extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }
}
