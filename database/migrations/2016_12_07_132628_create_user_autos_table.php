<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_autos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('auto_brand_id');
            $table->integer('auto_generation_id');
            $table->integer('auto_models_id');
            $table->integer('auto_modification_id');
            $table->integer('auto_series_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_autos');
    }
}
