<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_modifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('series_id');
            $table->integer('model_id');
            $table->string('modification_name');
            $table->string('start_production_year');
            $table->string('end_production_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_modifications');
    }
}
