<?php

use Illuminate\Database\Seeder;
use App\Models\AutoBrand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(public_path() . '/auto/1_brands.csv', "r");

        while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
            AutoBrand::create([
                'id' => $data[0],
                'brand_name' => $data[1]
            ]);
        }
    }
}
