<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BrandSeeder::class);
         $this->call(ModsSeeder::class);
         $this->call(GenerationSeeder::class);
         $this->call(SeriesSeeder::class);
         $this->call(ModificationsSeeder::class);
    }
}
