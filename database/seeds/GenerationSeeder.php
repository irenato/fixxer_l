<?php

use Illuminate\Database\Seeder;
use App\Models\AutoGeneration;

class GenerationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(public_path() . '/auto/3_generations.csv', "r");

        while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
//            var_dump($data);
            AutoGeneration::create([
                'id' => $data[0],
                'generation_name' => $data[1],
                'model_id' => $data[2],
                'year_begin' => $data[3],
                'year_end' => $data[4],
            ]);
        }
    }
}
