<?php

use Illuminate\Database\Seeder;
use App\Models\AutoModification;

class ModificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(public_path() . '/auto/5_modifications.csv', "r");

        while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
            AutoModification::create([
                'id' => $data[0],
                'series_id' => $data[1],
                'mod_id' => $data[2],
                'modification_name' => $data[3],
                'start_production_year' => $data[4],
                'end_production_year' => $data[5]
            ]);
        }
    }
}
