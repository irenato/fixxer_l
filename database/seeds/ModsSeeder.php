<?php

use Illuminate\Database\Seeder;
use App\Models\AutoMod;

class ModsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(public_path() . '/auto/2_models.csv', "r");

        while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
            AutoMod::create([
                'id' => $data[0],
                'brand_id' => $data[1],
                'model_name' => $data[2]
            ]);
        }
    }
}
