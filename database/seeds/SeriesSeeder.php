<?php

use Illuminate\Database\Seeder;
use App\Models\AutoSeries;

class SeriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(public_path() . '/auto/4_series.csv', "r");

        while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
            AutoSeries::create([
                'id' => $data[0],
                'model_id' => $data[1],
                'series_name' => $data[2],
            ]);
        }
    }
}
