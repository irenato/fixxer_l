<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AutoFix</title>
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/bootstrap-grid-3.3.1.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/avtoFix.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>

<body>
<!--START HEADER-->
<header class="landing-page">
    <div class="container">
        <div class="row">
            <div class="wrapper">
                <a class="logo" href="/">
                    <img src="images/LOGO.png" alt="logo">
                </a>
                <div class="right-side">
                    <div class="lang">

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Рус
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Eng</a></li>
                                <li><a href="#">Укр</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="enter open-modal" data-modal="enter">
                        <a href="#">ВХОД</a>
                    </div>
                    <div class="button green open-modal" data-modal="clientReg">
                        <a href="#"><span>НУЖЕН РЕМОНТ</span></a>
                    </div>
                    <div class="button transparent  open-modal" data-modal="masterReg">
                        <a href="#"><span>СТАТЬ МАСТЕРОМ</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--END HEADER-->

    <!--START CONTENT-->
    <main>
        <section class="section-top">
            <div class="banner">
                <div class="container">
                    <div class="row">
                        <div class="baner-wrapper">
                            <div class="banner-inner">
                                <h1>ПЕРВЫЙ СЕРВИС ДЛЯ СОТРУДНИЧЕСТВА СТО И АВТОВЛАДЕЛЬЦЕВ</h1>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Поиск категории">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> По категории
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">По категории</a></li>
                                            <li><a href="#">По мастерам</a></li>
                                        </ul>
                                    </div>
                                    <span class="input-group-btn">
									<button class="btn btn-secondary" type="button">
									  <i class="fa fa-search" aria-hidden="true"></i>
									</button>
								  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <div class="masters">
            <div class="container">
                <div class="row">


                    <h2>ЛУЧШИЕ МАСТЕРА <span class="in-category"></span><span class="in-city"></span></h2>
                    <div class="parameters">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-10">
                            <div class="btn-group city">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все регионы
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Eng</a></li>
                                    <li><a href="#">Укр</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-4 col-xs-10">
                            <div class="btn-group category">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все категории
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Общая диагностика</a></li>
                                    <li><a href="#">Электроника</a></li>
                                    <li><a href="#">Рихтовка</a></li>
                                    <li><a href="#">Сварка</a></li>
                                    <li><a href="#">Покраска</a></li>
                                    <li><a href="#">Шиномантаж</a></li>
                                    <li><a href="#">Химчистка</a></li>
                                    <li><a href="#">Реставрация салона</a></li>
                                    <li><a href="#">Тюнинг</a></li>
                                    <li><a href="#">Медвежатники</a></li>
                                    <li><a href="#">Мастер на выезд</a></li>
                                    <li><a href="#">Аренда инструментов</a></li>
                                    <li><a href="#">Эвакуаторы</a></li>
                                    <li><a href="#">Предпродажная подготовка</a></li>
                                    <li><a href="#">Другие услуги</a></li>
                                    <li><a href="#">Ремонт двигателей</a></li>
                                    <li><a href="#">Ремонт ходовой</a></li>
                                    <li><a href="#">Аренда помещения под ремонт</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="masters-slider">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                            <div class="master">
                                <img src="images/photo.png" alt="photo">
                                <div class="name-sto">СТО «100»</div>
                                <ul class="stars">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <div class="evaluation">
                                    (<span>150</span> оценок)
                                </div>
                                <div class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="city-location">Харьков</span>
                                </div>
                                <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="advantages">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-3 col-xs-6">
                        <div class="advantage">
                            <img src="images/advant.png" alt="photo">
                            <h3>ПРЕИМУЩЕСТВО</h3>
                            <div class="description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-xs-6">
                        <div class="advantage">
                            <img src="images/advant.png" alt="photo">
                            <h3>ПРЕИМУЩЕСТВО</h3>
                            <div class="description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-xs-6">
                        <div class="advantage">
                            <img src="images/advant.png" alt="photo">
                            <h3>ПРЕИМУЩЕСТВО</h3>
                            <div class="description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-xs-6">
                        <div class="advantage">
                            <img src="images/advant.png" alt="photo">
                            <h3>ПРЕИМУЩЕСТВО</h3>
                            <div class="description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="last-orders">
            <div class="container">
                <div class="row">
                    <h2>ПОСЛЕДНИЕ ЗАКАЗЫ</h2>
                    <div class="parameters">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-10">
                            <div class="btn-group city">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все регионы
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Eng</a></li>
                                    <li><a href="#">Укр</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-4 col-xs-10">
                            <div class="btn-group category">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все категории
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Общая диагностика</a></li>
                                    <li><a href="#">Электроника</a></li>
                                    <li><a href="#">Рихтовка</a></li>
                                    <li><a href="#">Сварка</a></li>
                                    <li><a href="#">Покраска</a></li>
                                    <li><a href="#">Шиномантаж</a></li>
                                    <li><a href="#">Химчистка</a></li>
                                    <li><a href="#">Реставрация салона</a></li>
                                    <li><a href="#">Тюнинг</a></li>
                                    <li><a href="#">Медвежатники</a></li>
                                    <li><a href="#">Мастер на выезд</a></li>
                                    <li><a href="#">Аренда инструментов</a></li>
                                    <li><a href="#">Эвакуаторы</a></li>
                                    <li><a href="#">Предпродажная подготовка</a></li>
                                    <li><a href="#">Другие услуги</a></li>
                                    <li><a href="#">Ремонт двигателей</a></li>
                                    <li><a href="#">Ремонт ходовой</a></li>
                                    <li><a href="#">Аренда помещения под ремонт</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="orders-list">
                        <div class="order">
                            <div class="order-slatus urgent">
                                <img src="images/fire.png" alt="fire">
                                <span>Срочный</span>
                            </div>
                            <div class="left-side">
                                <h4>Ремонт двигателя автомобиля</h4>
                                <p class="description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    <a href="#">Подробнее</a>
                                </p>
                                <ul class="order-info">
                                    <li class="relevant">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>Актуален&shy; <span class="days"> 6 дней &shy;</span> <span class="hour">18 часов</span>
                                    </li>
                                    <li class="location">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>Харьков
                                    </li>
                                </ul>
                            </div>
                            <div class="right-side">
                                <div class="cost">
                                    <span class="budget">Бюджет: </span>
                                    <span class="sum">5000 грн.</span>
                                </div>
                                <div class="button green">
                                    <a href="#">
                                        <span>ВЫПОЛНИТЬ</span>
                                    </a>
                                </div>
                                <div class="user">
                                    <span class="user-name">Сидор Вишевский</span>
                                    <img src="images/user-photo.png" alt="user-photo" class="user-hpoto">
                                </div>
                            </div>
                        </div>
                        <div class="order">
                            <div class="order-slatus">
                                <img src="images/fire.png" alt="fire">
                                <span>Срочный</span>
                            </div>
                            <div class="left-side">
                                <h4>Ремонт двигателя автомобиля</h4>
                                <p class="description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    <a href="#">Подробнее</a>
                                </p>
                                <ul class="order-info">
                                    <li class="relevant">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>Актуален&shy; <span class="days"> 6 дней &shy;</span> <span class="hour">18 часов</span>
                                    </li>
                                    <li class="location">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>Харьков
                                    </li>
                                </ul>
                            </div>
                            <div class="right-side">
                                <div class="cost">
                                    <span class="budget">Бюджет: </span>
                                    <span class="sum">5000 грн.</span>
                                </div>
                                <div class="button green">
                                    <a href="#">
                                        <span>ВЫПОЛНИТЬ</span>
                                    </a>
                                </div>
                                <div class="user">
                                    <span class="user-name">Сидор Вишевский</span>
                                    <img src="images/user-photo.png" alt="user-photo" class="user-hpoto">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button transparent grey"><a href="#"><span>ПОКАЗАТЬ БОЛЬШЕ</span></a></div>
                </div>
            </div>
        </div>
    </main>
    <!--END CONTENT-->

    <!--MODAL ENTER-->
@include('modals.login')

    <!--MODAL CLIENT REGISTRATION-->
@include('modals.register_client')
    <!--MODAL CHOOSE THE KIND OF REGISTRATION-->
@include('modals.registration_choose')
    <!--MODAL MASTER REGISTRATION-->
@include('modals.register_master')

<!--START FOOTER-->
<footer class="landing-page">

    <div class="footer-top">
        <div class="container">
            <div class="row">
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КОНТАКТЫ</span>
                    <li>Номер телефона и тд и тп</li>
                    <li>Электронная почта</li>
                    <li>Адрес, страна и тд и тп</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="left-side">
                    <a class="logo" href="/">
                        <img src="images/LOGO-blue.png" alt="LOGO-blue">
                    </a>
                    <div class="social">
                        <div class="facebook">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </div>
                        <div class="vk">
                            <i class="fa fa-vk" aria-hidden="true"></i>
                        </div>
                        <div class="ok">
                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <span>Все права защищены 2016</span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--END FOOTER-->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/common.js"></script>

</body>

</html>