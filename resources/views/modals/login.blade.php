<div class="modal-win enter">
    <div class="container">
        <div class="row">

            <h2>ВХОД НА САЙТ</h2>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <h3>Вход через соц.сети</h3>
                <div class="social">
                    <div class="social-inner">
                        <div class="facebook">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </div>
                        <div class="vk">
                            <i class="fa fa-vk" aria-hidden="true"></i>
                        </div>
                        <div class="ok">
                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="line-bt"></div>
                <div class="input-field">
                    <input type="email" placeholder="E-mail" required>
                </div>
                <div class="input-field">
                    <input type="password" placeholder="Пароль" required>
                </div>
                <div class="forgot-pasword">
                    <a href="#">Забыли пароль?</a>
                </div>
                <div class="button green">
                    <input type="submit" value="ВОЙТИ">
                </div>
                <div class="line-bt"></div>
                <span>Ещё не зарегестрированы? <a href="kind-of-registration" class="continue">Вперёд!</a></span>
            </form>
        </div>
    </div>
</div>