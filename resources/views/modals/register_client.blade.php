<div class="modal-win clientReg">
    <div class="container">
        <div class="row">

            <h2>Регистрация клиента</h2>
            <h3>Зарегестрируйтесь как клиент, чтобы иметь возможность записываться на ремонт в лучшие СТО и выкладывать объявления о ремонте авто</h3>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <div class="form-header">
                    <div class="step active">
                        <div class="general-info"><span>1</span></div>
                        <p>Общая информация</p>
                    </div>
                    <div class="step step2">
                        <div class="client-info"><span>2</span></div>
                        <p>Информация о клиенте</p>
                    </div>
                    <span class="progress"></span>
                </div>
                <div class="form-content">
                    <div id="content1" class="general-info content">
                        <div class="input-field">
                            <input type="email" name="email" placeholder="E-mail*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" placeholder="Пароль*" required>
                        </div>
                        <div class="input-field">
                            <input type="tel" name="tel" placeholder="Телефон*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="iteratePassword" placeholder="Подтвердить пароль*" required>
                        </div>
                        <div class="button green"><a href="#content2" class="toggle"><span>ДАЛЕЕ</span></a></div>
                    </div>


                    <!-- ----------------------- content2 --------------------------->
                    <div id="content2" class="client-info content">
                        <div class="photo">
                            <input type="file" id="file" name="file" style="display:none;" />
                            <div class="client-photo">
                                <span id="output"></span>
                            </div>
                            <h4>Ваша фотография <br>(необязательно)</h4>
                        </div>
                        <div class="input-field">
                            <input type="text" name="firstName" placeholder="Имя*" required>
                        </div>
                        <div class="input-field">
                            <input type="text" name="lastName" placeholder="Фамилия*" required>
                        </div>
                        <div class="btn-group area">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Ваша область*
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">some text</a></li>
                                <li><a href="#">some text</a></li>
                            </ul>
                        </div>

                        <div class="button grey"><a href="#content1" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green">
                            <input type="submit" value="ЗАРЕГЕСТРИРОВАТЬСЯ">
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <span class="title-social">Регистрация через соц. сети &nbsp;</span>
                    <div class="social">
                        <div class="social-inner">
                            <div class="facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div class="vk">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </div>
                            <div class="ok">
                                <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <span>Уже зарегестрированы?<a href="enter" class="continue"> Войти! </a></span>
                </div>


            </form>
        </div>
    </div>
</div>