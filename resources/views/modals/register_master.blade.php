<div class="modal-win masterReg">
    <div class="container">
        <div class="row">

            <h2>Регистрация СТО/Мастера</h2>
            <h3>Зарегестрируйтесь как СТО/Мастер и берите заказы от автовладельцев и осуществляйте эффективный трекинг заказов через наш сайт</h3>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <div class="form-header">
                    <div class="step active">
                        <div class="general-info"><span>1</span></div>
                        <p>Общая информация</p>
                    </div>
                    <div class="step step2">
                        <div class="client-info"><span>2</span></div>
                        <p class="master">Информация о Мастере</p>
                        <p class="sto" hidden>Информация об СТО</p>
                    </div>
                    <span class="progress"></span>
                    <div class="step step3">
                        <div class="client-info"><span>3</span></div>
                        <p>Навыки</p>
                    </div>
                    <span class="progress progress2"></span>
                </div>
                <div class="form-content">
                    <div id="masterContent1" class="general-info content">
                        <div class="input-group inline">
                            <h3>Выберите ваш вид деятельности:</h3>

                            <input type="radio" name="optradio" id="master" hidden checked>
                            <label class="radio-inline" for="master">Мастер</label>

                            <input type="radio" name="optradio" id="sto" hidden>
                            <label class="radio-inline" for="sto">СТО</label>
                        </div>
                        <div class="input-field">
                            <input type="email" name="email" placeholder="E-mail*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" placeholder="Пароль*" required>
                        </div>
                        <div class="input-field tel">
                            <input type="tel" name="tel" placeholder="Телефон*" required>
                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                        </div>
                        <div class="input-field">
                            <input type="password" name="iteratePassword" placeholder="Подтвердить пароль*" required>
                        </div>
                        <div class="clearfix"></div>
                        <div class="addedTel"></div>
                        <p class="addTelNumber">Добавить доп. номер телефона <i class="fa fa-plus" aria-hidden="true"></i></p>
                        <div class="button green"><a href="#masterContent2"  class="toggle"><span>ДАЛЕЕ</span></a></div>
                    </div>


                    <!-- ----------------------- content2 --------------------------->
                    <div id="masterContent2" class="client-info content">
                        <div class="sto masterContent2Optional">
                            <div class="photo">
                                <input type="file" id="file" name="file" style="display:none;" />
                                <div class="sto-photo">
                                    <span id="output"></span>
                                </div>
                                <h4>Логотип СТО <br>(необязательно)</h4>
                            </div>
                            <div class="input-field">
                                <input type="text" name="firstName" placeholder="Название СТО*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="area" placeholder="Область*" required>
                            </div>
                            <div class="input-field schedule">
                                <span>Расписание*:</span>
                                <input type="text" name="days" class="days" placeholder="Дни*" required>
                                <input type="text" name="hours" class="hours" placeholder="Время*" required>

                                <div class="drop-down days">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">Пн</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                            <span class="name">Пт</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                    </div>
                                </div>

                                <div class="drop-down hours">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">9:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">18:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                        <input type="checkbox" class="roundClock" id="roundClockSto" hidden="">
                                        <label for="roundClockSto">Круглосуточно</label>
                                    </div>
                                </div>

                            </div>
                            <div class="input-field">
                                <input type="text" name="city" placeholder="Город*" required>
                            </div>
                            <div class="input-field">
                            </div>
                            <div class="input-field addres">
                                <input type="text" name="street" placeholder="Улица*" required>
                                <input type="text" name="house" placeholder="Дом*" required>
                            </div>
                            <div class="clearfix"></div>
                            <div class="addedAdres">
                            </div>

                            <p class="addAddres">Добавить доп. адрес <i class="fa fa-plus" aria-hidden="true"></i></p>
                            <textarea name="description" placeholder="Описание"></textarea>
                        </div>
                        <div class="master masterContent2Optional">
                            <div class="photo">
                                <input type="file" id="file" name="file" style="display:none;" />
                                <div class="client-photo">
                                    <span id="output"></span>
                                </div>
                                <h4>Ваша фотография <br>(необязательно)</h4>
                            </div>
                            <div class="input-field">
                                <input type="text" name="firstName" placeholder="Имя*" required>
                            </div>

                            <div class="input-field">
                                <input type="text" name="area" placeholder="Область*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="lastName" placeholder="Фамилия*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="city" placeholder="Город*" required>
                            </div>
                            <div class="input-field schedule">
                                <span>Расписание*:</span>
                                <input type="text" name="days" class="days" placeholder="Дни*" required>
                                <input type="text" name="hours" class="hours" placeholder="Время*" required>

                                <div class="drop-down days">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">Пн</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                            <span class="name">Пт</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                    </div>
                                </div>

                                <div class="drop-down hours">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">9:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">18:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                        <input type="checkbox" class="roundClock" id="roundClockMaster" hidden="">
                                        <label for="roundClockMaster">Круглосуточно</label>
                                    </div>
                                </div>

                            </div>
                            <div class="input-field addres">
                                <input type="text" name="street" placeholder="Улица*" required>
                                <input type="text" name="house" placeholder="Дом*" required>
                            </div>
                            <div class="clearfix"></div>
                            <div class="addedAdres">
                            </div>
                            <p class="addAddres">Добавить доп. адрес <i class="fa fa-plus" aria-hidden="true"></i></p>
                            <textarea name="description" placeholder="Описание"></textarea>
                        </div>
                        <div class="button grey"><a href="#masterContent1" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green"><a href="#masterContent3" class="toggle"><span>
								ДАЛЕЕ
							</span></a></div>
                    </div>

                    <!-- ----------------------- content3 --------------------------->
                    <div id="masterContent3" class="client-info content">

                        <div class="skills">
                            <div class="skills-header">
                                <h3>Выберите ваши навыки</h3>
                                <div class="counter">Выбрано <span class="chosen">0</span>/<span class="allCounter">15</span> навыков</div>
                            </div>
                            <div class="skills-body">
                                <ul>
                                    <li>Общая диагностика<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Электроника<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Рихтовка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Сварка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Покраска<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Шиномантаж<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Химчистка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Реставрация салона<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Тюнинг<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Медвежатники<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Мастер на выезд<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Аренда инструментов<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Эвакуаторы<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Другие услуги<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Предпродажная подготовка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Ремонт двигателей<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Ремонт ходовой<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Аренда помещения под ремонт<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="button grey"><a href="#masterContent2" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green">
                            <input type="submit" value="ЗАРЕГЕСТРИРОВАТЬСЯ">
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <span class="title-social">Регистрация через соц. сети &nbsp;</span>
                    <div class="social">
                        <div class="social-inner">
                            <div class="facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div class="vk">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </div>
                            <div class="ok">
                                <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <span>Уже зарегестрированы?<a href="enter" class="continue"> Войти! </a></span>
                </div>


            </form>
        </div>
    </div>
</div>