<div class="modal-win kind-of-registration">
    <div class="container">
        <div class="row">

            <h2>Регистрация</h2>
            <h3>Выберите вид регистрации:</h3>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <div class="inner-win">
                <div class="left-side">

                    <i class="icon-businessman" aria-hidden="true"></i>
                    <h3>Клиент</h3>
                    <p>Найдите мастера для устранения поломок вашего авто</p>
                    <div class="button green">
                        <a href="clientReg" class="continue">Далее</a>
                    </div>
                </div>
                <div class="right-side">

                    <i class="icon-mechanic" aria-hidden="true"></i>
                    <h3>Мастер</h3>
                    <p>Поиск клиентов для принятия заказа на ремонт авто</p>
                    <div class="button green">
                        <a href="masterReg" class="continue">Далее</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>